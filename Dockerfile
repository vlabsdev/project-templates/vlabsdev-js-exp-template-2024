
# Dockerfile to build and run the module on the cloud
# Stage 1 :- node build simulation

FROM node:20.9.0 AS nodebuilder
WORKDIR /usr/src/app
RUN apt-get update && apt-get install -y \
    zip unzip
COPY . .
COPY package*.json ./
COPY tsconfig*.json ./
ARG WGET_PULL
RUN wget -q "https://gitlab.com/api/v4/projects/51662972/repository/archive.zip?private_token=$WGET_PULL&ref=developer"
RUN unzip archive.zip*
RUN rm archive.zip*
RUN mv math*/* /usr/src/app
RUN mv css /usr/src/app/simulation/
RUN rm -r math*
RUN npm ci --quiet && npm run build

# Stage 2 :- vlabsdev template integration

FROM python:3.9 AS templatebuilder
ARG WGET_PULL
ARG CDN_LINK
RUN apt-get update
RUN apt-get install wget
RUN apt-get install zip unzip -y
RUN mkdir app
WORKDIR /app
COPY requirements.txt /app
RUN pip install --no-cache-dir --upgrade -r requirements.txt
RUN wget -q "https://gitlab.com/api/v4/projects/29020733/repository/archive.zip?private_token=$WGET_PULL"
RUN unzip archive.zip*
RUN rm archive.zip*
RUN mv vlabsdev*/* /app
COPY lab-manual /app/lab-manual
RUN rm -r vlabsdev*
COPY --from=nodebuilder /usr/src/app/simulation /app/simulation
RUN python3 script.py

# Dockerfile to build and run the module on the cloud
# Stage 3 s3 push
FROM d3fk/s3cmd AS s3-bucket
ARG S3_AKEY
ARG S3_SKEY
ARG PROJECT_NAME
ARG NODAL_NAME
ARG S3_LAB_MANUAL
ARG S3_ACTIVITY
ARG S3_ACL
ARG S3_HOST
ARG CDN_LINK

# Pushing BUILD CONTEXT to CDN
RUN mkdir -p /home/$NODAL_NAME/$PROJECT_NAME
WORKDIR /home
COPY --from=templatebuilder /app /home/$NODAL_NAME/$PROJECT_NAME
RUN s3cmd put --recursive /home/ $S3_LAB_MANUAL/ --host-bucket=bucket --host=$S3_HOST --no-check-certificate --access_key=$S3_AKEY --secret_key=$S3_SKEY

