# JS based Simulation Experiment.

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

## Add your files

-  git clone the project locally.
-  Switch to `developer` branch
-  Add the simulation files under the `simulation` folder.
-  Add the ts simulation files under `ts_simulation` folder
-  Add the lab manual files under the `lab-manual` folder. It also has `details.json` Edit it with appropriate author/contributor name and ame of exp.
-  Do not change any other file, file names and file content in the root of this project. 
-  Do not rename folders in this project template.
-  add following files/directories under `.gitignore` before pushing the changes.

```
simulation/js/
ts_components/
ts_math_library/
simulation/css
simulation/edit.html
node_modules/

```

## Hosting / Deploying your activity.
-  After pushing all the files to developer branch , raise a Merge Request (MR) from `developer`branch to the `main` branch.
-  `Merge to MAIN. `
-  Once the build pipeline passes (~5-15 mins), it will be `auto-deployed` under the url `https://vlabsdev.in/labs/lab-name/exp-id/ `
-  for eg: if the `lab-name : numerical-methods` and the `exp-id: d1-l8-e1`then the deployment URL will be  `https://vlabsdev.in/labs/numerical-methods/d1-l8-e1/`

